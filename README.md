# Docker image "Hola mundo"

## clonar

```sh
mkdir -p Workspace/afip/
cd Workspace/afip/
git clone https://gitlab.com/gustavo.zalazar/hola-mundo-httpd.git
```

## build

```sh
cd Workspace/afip/hola-mundo-httpd
```

## execute

```sh
docker run -dit -p80:80 mikroways/hola‑mundo‑httd
```

## test

curl http://localhost/